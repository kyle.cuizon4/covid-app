import toNum from '../helpers/toNum'
import Jumbotron from 'react-bootstrap/Jumbotron' 
import CovidMap from '../components/CovidMap'

export default function Home({globalTotal}) {
  return (
    <>
      <Jumbotron>
        <h1>Total Covid-19 cases in the world: <strong>{globalTotal.cases}</strong></h1>
      </Jumbotron>
      <CovidMap/>
    </>
  )
}

export async function getStaticProps() {
  //fetch data from the /courses API endpoint    
  const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
    "method": "GET",
    "headers": {
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
      //share the value of this property to the class
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
    }
  })
  const data = await res.json()
  const countriesStats = data.countries_stat

  let total = 0
  countriesStats.forEach(country => {
    total += toNum(country.cases)
  })

  const globalTotal = {
    cases: total
  }

  //return the props
  return { 
      props: {
          globalTotal
      }
  }
}
