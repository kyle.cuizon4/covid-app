import toNum from '../../../helpers/toNum'
import DoughnutChart from '../../../components/DoughnutChart'
import Banner from '../../../components/Banner'
import GlobalMap from '../../../components/GlobalMap'
import { Row, Col } from 'react-bootstrap'

export default function country({ country }) {
    console.log(country)
    return (
        <>
            <Banner country={country.country_name} deaths={country.deaths} criticals={country.serious_critical} recoveries={country.total_recovered} />

            <Row>
                <Col xs={12} md={6}>
                    <DoughnutChart criticals={toNum(country.serious_critical)} deaths={toNum(country.deaths)} recoveries={toNum(country.total_recovered)}/>    
                </Col>
                <Col xs={12} md={6}>
                    <GlobalMap country={country}/>
                </Col>
            </Row>

        </>
    )
}

export async function getStaticPaths(){
    const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
        "method": "GET",
        "headers": {
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
        "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
        }
    })
    const data = await res.json()

    const paths = data.countries_stat.map(country => ({
        params: { id: country.country_name }
    }))

    return { paths, fallback:false }
}

export async function getStaticProps({params}){
    const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
        "method": "GET",
        "headers": {
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
        "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
        }
    })
    const data = await res.json()

    const country = data.countries_stat.find(country => country.country_name === params.id)

    return {
        props: {
            country
        }
    }
}
