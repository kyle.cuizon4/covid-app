import ListGroup from 'react-bootstrap/ListGroup'
import Head from 'next/head'

export default function index({data}){
	console.log(data)
	// console.log(data.countries_stat)
	const countriesList = data.countries_stat.map(country => {
		return (
				<ListGroup.Item key={country.country_name}>
					<a href={`/covid/countries/${country.country_name}`}>{country.country_name}</a>
				</ListGroup.Item>
			)
	})

	return (
		<>
		<Head>
			<title>
				Covid 19 Infected Countries
			</title>
		</Head>
		<ListGroup>
			{countriesList}
		</ListGroup>
		</>
		)
}

export async function getStaticProps(){
	const res = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php',{
    "method" : "GET",
    "headers": {
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
    }
  })

  const data = await res.json()

  return {
  	props: {
  		data
  	}
  }
}