import {Doughnut} from 'react-chartjs-2'
import toNum from '../../helpers/toNum'
import {useEffect, useRef, useState} from 'react'

import mapboxgl from 'mapbox-gl'
mapboxgl.accessToken = 'pk.eyJ1IjoiY3Vpemt5bGU0IiwiYSI6ImNraHR4cWt1ODBvaDAycHQ5dDhyNzIydG8ifQ.7xelvhEYTgM8SVsRgcdhpA'


export default function top({data}){
	
	const mapContainerRef = useRef(null)


	const countriesStats = data.countries_stat
	const countriesCases = countriesStats.map(country => {
		// console.log(country)
		return {
			name: country.country_name,
			cases: toNum(country.cases)
		}
	})


	// console.log(countriesCases)
	//sort countries cases in descending order
	countriesCases.sort((currEl, nextEl)=>{
		// console.log(currEl.cases, nextEl.cases)
		if (currEl.cases < nextEl.cases) {
			return 1
		} else if(currEl.cases > nextEl.cases){
			return -1
		} else {
			return 0
		}
	})

	useEffect(() => {
		const map = new mapboxgl.Map({
			container : mapContainerRef.current,
			style : 'mapbox://styles/mapbox/streets-v11',
			center : [44.63,28.77],
			zoom : 0
		})
		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

		for(let i = 0; i < 10 ; i++){
			fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${countriesCases[i].name}.json?access_token=${process.env.NEXT_PUBLIC_REACT_MAPBOX_KEY}`)
			.then(res => res.json())
			.then(data => {
				new mapboxgl.Marker()
				.setLngLat([data.features[0].center[0], data.features[0].center[1]])
				.addTo(map)
			})
		}

		return  () => map.remove()
	},[])

	// console.log(countriesCases[0].cases)
	return(
		<>
		<h1>Top 10 Countries with the highest number of cases</h1>
			<Doughnut
				data={{
					datasets:[{
						data:[countriesCases[0].cases, countriesCases[1].cases, countriesCases[2].cases, countriesCases[3].cases, countriesCases[4].cases, countriesCases[5].cases, countriesCases[6].cases, countriesCases[7].cases, countriesCases[8].cases, countriesCases[9].cases],
						backgroundColor: ["blue", "red", "orange", "gray", "black", "indianred","yellow", "pink", "purple", "salmonred"]
					}],
					labels:[countriesCases[0].name, countriesCases[1].name, countriesCases[2].name, countriesCases[3].name, countriesCases[4].name, countriesCases[5].name, countriesCases[6].name, countriesCases[7].name, countriesCases[8].name, countriesCases[9].name]
				}}
				redraw={false}
			/>

			<div className="mapContainer" ref={mapContainerRef} />
		</>
		)
}

export async function getStaticProps(){
	/*fetch data from the RAPID API Endpoint*/
	const res = await fetch('https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php',{
    "method" : "GET",
    "headers": {
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
    }
  })
  	const data = await res.json()

  

  	return {
  		props: {
  			data
  			
  		}
  	}
}