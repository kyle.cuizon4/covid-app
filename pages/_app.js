import NavBar from '../components/NavBar'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container} from 'react-bootstrap'
import '../styles.css'
import 'mapbox-gl/dist/mapbox-gl.css'

function MyApp({ Component, pageProps }) {
  return (
    <>
	      <NavBar />
	      <Container>
		      <Component {...pageProps} />
	      </Container>
    </>
  )
    
}

export default MyApp
