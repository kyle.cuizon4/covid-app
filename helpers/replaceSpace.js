export default function replaceSpace(str){
    let arr = [...str]
    let newArr = arr.map( char => {
        return char === ' ' ? '%20' : char
    } )
    return newArr.join('')
}