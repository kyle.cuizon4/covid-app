export default function toNum(str){
    const arr = [...str]
    const filteredArr = arr.filter( elem => elem !== ",")
    return parseInt(filteredArr.reduce((x,y) => x+y ))
}
