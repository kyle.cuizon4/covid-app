import {useEffect} from 'react'
export default function CovidMap(){

useEffect(()=>{
	const script = document.createElement('script')
	script.src = "https://apps.elfsight.com/p/platform.js"
	script.async = true

	document.body.appendChild(script)

	//cleanup after unmount
	return () => {
		document.body.removeChild(script)
	}
}, [])

	return(
		<>
			<div className="elfsight-app-f8c687b7-e4b0-4653-8083-588fcb66eb1d"></div>
		</>
		)
}