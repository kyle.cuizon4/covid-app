import { useEffect, useRef, useState } from 'react'
import mapboxgl from 'mapbox-gl'
mapboxgl.accessToken = 'pk.eyJ1IjoiY3Vpemt5bGU0IiwiYSI6ImNraHR4cWt1ODBvaDAycHQ5dDhyNzIydG8ifQ.7xelvhEYTgM8SVsRgcdhpA'

const GlobalMap = (data) => {
	console.log(data)
	const mapContainerRef = useRef(null)
	const [latitude, setLatitude] = useState(0)
	const [longitude, setLongitude] = useState(0)
	const [zoom, setZoom] = useState(0)
 
	useEffect(() => {

		fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${data.country.country_name}.json?access_token=${process.env.NEXT_PUBLIC_REACT_MAPBOX_KEY}`)
		.then(res => res.json())
		.then(data => {
			setLongitude(data.features[0].center[0])
			setLatitude(data.features[0].center[1])
			setZoom(2)
		})

		const map = new mapboxgl.Map({
			container: mapContainerRef.current,
			style: 'mapbox://styles/mapbox/streets-v11',
			center: [longitude, latitude],
			zoom: zoom
		})

		const marker = new mapboxgl.Marker()
		.setLngLat([longitude, latitude])
		.addTo(map)

		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right')

		return () => map.remove()
	}, [latitude, longitude])

	return <div className="mapContainer" ref={mapContainerRef}/>
}

export default GlobalMap
