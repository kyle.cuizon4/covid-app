import {Doughnut} from 'react-chartjs-2'

export default function DoughnutChart({criticals, active_cases, deaths}){
	return(
		<Doughnut
			data={{
				datasets:[{
					data: [criticals, deaths, active_cases ],
					backgroundColor: ["red", "black","yellow"]
				}],
				labels: [
					'Criticals',
					'Deaths',
					'Active Cases'
				]
			}}
		/>
		)
}