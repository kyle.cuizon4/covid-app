import Jumbotron from 'react-bootstrap/Jumbotron'
import Button from 'react-bootstrap/Button'
import Link from 'next/link'

export default function Banner({ country, criticals, deaths, recoveries }) {
    return (
        <Jumbotron>
            <h1>{country}</h1>
            <p>Deaths: {deaths}</p>
            <p>Recoveries: {recoveries}</p>
            <p>Critical cases: {criticals}</p>
            <Button variant="success">
                <Link href="/covid/countries">
                    <a>View Countries</a>
                </Link>
            </Button>
        </Jumbotron>
    )
}
